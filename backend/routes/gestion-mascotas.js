var ENCRIPTACION = require("../routes/utilidades/encriptacion");
var DBCONEXION = require('../mod/conexion');
var conexion_db_ON;
var GESTIONMASCOTA = require('../mod/gestion-mascotas');
var TIPOMASCOTA = require('../mod/tipo-mascota');
var express = require('express');
var router = express.Router();
FILECONTROLADOR = {};

FILECONTROLADOR.iniciar = function (socket) {

	router.get('/get-mascotas-con-dueno', function(req, res) {
		try {
			DBCONEXION.iniciar_conexion(function (conexion_db) {
				if (conexion_db!=false) {
					conexion_db_ON = conexion_db;

                    GESTIONMASCOTA.get_mascotas_con_duenos(conexion_db_ON, function (result_1) {
                        if(!result_1)
                        {
                            res.json({
                                error:true,
                                mensaje:"Todavia no se han realizado registros",
                            });
                        }else{
                            
                            res.json({
                                datos: result_1,
                            });
                        };
                    });

				}
			});
		}catch (error) {
			console.log(error)
		}
	});    

	router.post('/get-duenos', function(req, res) {
		try {
			DBCONEXION.iniciar_conexion(function (conexion_db) {
				if (conexion_db!=false) {
					conexion_db_ON = conexion_db;
					GESTIONMASCOTA.get_duenos(conexion_db_ON,function (result) {
						if(!result)
						{
							res.json({
								error:true,
								mensaje:"Error consultando"
							});
						}else{
							res.json({
								error:false,
								datos: result,
								mensaje: (result[0])? '' : 'Todavia no se han realizado registros'
							});
						};
					});
				}
			});
		}catch (error) {
			console.log(error)
		}
	});
	router.post('/get-mascotas', function(req, res) {
        console.log("ff")
		try {
			DBCONEXION.iniciar_conexion(function (conexion_db) {
				if (conexion_db!=false) {
					conexion_db_ON = conexion_db;
                    TIPOMASCOTA.get_tipos_mascota(conexion_db_ON,function (result) {
						if(result==false)
						{
							res.json({
								error:true,
								mensaje:"Error consultando"
							});
						}else{

                            GESTIONMASCOTA.get_mascotas(conexion_db_ON, req.body, function (result_1) {
                                if(result_1==false)
                                {
                                    res.json({
                                        error:true,
                                        mensaje:"Error consultando",
                                        tipos_mascota: result
                                    });
                                }else{
                                    
                                    res.json({
                                        error:false,
                                        tipos_mascota: result,
                                        datos: result_1,
                                        mensaje: (result_1[0])? '' : 'Todavia no se han realizado registros'
                                    });
                                };
                            });

						};
					});
				}
			});
		}catch (error) {
			console.log(error)
		}
	});    
	router.post('/registrar-dueno', function(req, res) {
		try {

            if(req.body.token){
				let datosToken_ = JSON.parse(ENCRIPTACION.decryptMSS(req.body.token));
				if (datosToken_.id>0) {

                    DBCONEXION.iniciar_conexion(function (conexion_db) {
                        if (conexion_db!=false) {
                            conexion_db_ON = conexion_db;
                            GESTIONMASCOTA.verificarEmailDueno(conexion_db_ON,req.body,function (err_v,result_v2) {
                                //console.log(err_v,result_v);
                                if(result_v2==false)
                                {	
                                    
                                    GESTIONMASCOTA.registrarDueno(conexion_db_ON,req.body,function (error) {
                                        if(error==true)
                                        {
                                            res.json({
                                                error:true,
                                                mensaje:"Error guardando"
                                            });
                                        }else{

                                            GESTIONMASCOTA.get_duenos(conexion_db_ON,function (result) {
                                                if(result==false)
                                                {
                                                    res.json({
                                                        error:true,
                                                        mensaje:"Error consultando"
                                                    });
                                                }else{
                                                    res.json({
                                                        error:false,
                                                        datos: result,
                                                        mensaj_0 : "¡Se ha realizado el registro!",
                                                        mensaje: (result[0])? '' : 'No se han realizado registros'
                        
                                                    });
                                                };
                                            });									

                                        };
                                    });

                                }else{
                                    res.json({
                                        error:true,
                                        mensaje:"Ya existe el dato"

                                    });						
                                }
                            });
                        }
                    });

                }else{
                    res_.json({
                        error:true,
                        mensaje : "No posee la permisologia, token invalido al desifrar",
                        token:datosToken_
                    });
                }
            }else{
                res_.json({
                    error:true,
                    mensaje : "No posee la permisologia, no hay token"
                });
            }            
		}catch (error) {
			console.log(error)
		}
	});
	router.post('/editar-dueno', function(req_, res_) {
		try {

			if(req_.body.token){
				let datosToken_ = JSON.parse(ENCRIPTACION.decryptMSS(req_.body.token));
				if (datosToken_.id>0) {
					DBCONEXION.iniciar_conexion(function (conexion_db) {
						if (conexion_db!=false) {
							conexion_db_ON = conexion_db;
							//console.log(datosToken_,req_.body.datos);
					
							GESTIONMASCOTA.verificarEmailDueno(conexion_db_ON,req_.body.datos,function (err_v1,result_v1) {
								//console.log(result_v1[0]);
								if(result_v1==false)
								{	

									GESTIONMASCOTA.editarDueno(conexion_db_ON,datosToken_,req_.body.datos,function(err_0,result_0){
										//console.log(err_0,result_0);
										if(result_0!=true)
										{
											if (err_0==true) {
												res_.json({
													error:true,
													error_m:err_0,
													mensaje:"Error de base de datos, comuniquese con su administrador"
												}); 
											} else {
												res_.json({
													error:true,
													error_m:"Error de base de datos, comuniquese con su administrador",
													mensaje:err_0
												}); 								
											}
											
										}else{


											GESTIONMASCOTA.get_duenos(conexion_db_ON,function (result) {
												if(result==false)
												{
													res.json({
														error:true,
														mensaje:"Error consultando"
													});
												}else{
													res.json({
														error:false,
														datos: result,
														mensaj_0 : "Guardado correctamente",
														mensaje: (result[0])? '' : 'No se han realizado registros'
						
													});
												};
											});

										}
									});
								
								}else{
									//DBCONEXION.cerrar_conexion(conexion_db_ON);
									res_.json({
										error:true,
										mensaje:"Ya existe el dato"

									});						
								}
							});																		

						}
					});
				}else{
					res_.json({
						error:true,
						mensaje : "No posee la permisologia, token invalido al desifrar",
						token:datosToken_
					});
	
				}
				
			}else{
				res_.json({
					error:true,
					mensaje : "No posee la permisologia, no hay token"
				});
			}

		}catch (error) {
			console.log(error)
		}
	});
	router.post('/registrar-mascota', function(req, res) {
		try {
    		console.log(req.body);
			DBCONEXION.iniciar_conexion(function (conexion_db) {
				if (conexion_db!=false) {
					conexion_db_ON = conexion_db;
					GESTIONMASCOTA.verificarNombreMascota(conexion_db_ON,req.body,function (err_v,result_v2) {
						//console.log(err_v,result_v);
						if(result_v2==false)
						{	
							
							GESTIONMASCOTA.registrarMascota(conexion_db_ON,req.body,function (error) {
								if(error==true)
								{
									res.json({
										error:true,
										mensaje:"Error guardando"
									});
								}else{

									GESTIONMASCOTA.get_mascotas(conexion_db_ON, req.body, function (result) {
										if(result==false)
										{
											res.json({
												error:true,
												mensaje:"Error consultando"
											});
										}else{
											res.json({
												error:false,
												datos: result,
												mensaj_0 : "¡Se ha realizado el registro!",
												mensaje: (result[0])? '' : 'No se han realizado registros'
				
											});
										};
									});									

								};
							});

						}else{
							res.json({
								error:true,
								mensaje:"Ya existe el dato"

							});						
						}
					});
				}
			});
		}catch (error) {
			console.log(error)
		}
	});
	router.post('/editar-mascota', function(req_, res_) {
		try {

			if(req_.body.token){
				let datosToken_ = JSON.parse(ENCRIPTACION.decryptMSS(req_.body.token));
				if (datosToken_.id>0) {
					DBCONEXION.iniciar_conexion(function (conexion_db) {
						if (conexion_db!=false) {
							conexion_db_ON = conexion_db;
							//console.log(datosToken_,req_.body.datos);
					
							GESTIONMASCOTA.verificarNombreMascota(conexion_db_ON,req_.body.datos,function (err_v1,result_v1) {
								//console.log(result_v1[0]);
								if(result_v1==false)
								{	

									GESTIONMASCOTA.editarMascota(conexion_db_ON,datosToken_,req_.body.datos,function(err_0,result_0){
										//console.log(err_0,result_0);
										if(result_0!=true)
										{
											if (err_0==true) {
												res_.json({
													error:true,
													error_m:err_0,
													mensaje:"Error de base de datos, comuniquese con su administrador"
												}); 
											} else {
												res_.json({
													error:true,
													error_m:"Error de base de datos, comuniquese con su administrador",
													mensaje:err_0
												}); 								
											}
											
										}else{


											GESTIONMASCOTA.get_mascotas(conexion_db_ON, req.body.datos, function (result) {
												if(result==false)
												{
													res.json({
														error:true,
														mensaje:"Error consultando"
													});
												}else{
													res.json({
														error:false,
														datos: result,
														mensaj_0 : "Guardado correctamente",
														mensaje: (result[0])? '' : 'No se han realizado registros'
						
													});
												};
											});

										}
									});
								
								}else{
									//DBCONEXION.cerrar_conexion(conexion_db_ON);
									res_.json({
										error:true,
										mensaje:"Ya existe el dato"

									});						
								}
							});																		

						}
					});
				}else{
					res_.json({
						error:true,
						mensaje : "No posee la permisologia, token invalido al desifrar",
						token:datosToken_
					});
	
				}
				
			}else{
				res_.json({
					error:true,
					mensaje : "No posee la permisologia, no hay token"
				});
			}

		}catch (error) {
			console.log(error)
		}
	});

	return router;
}	

module.exports = FILECONTROLADOR;