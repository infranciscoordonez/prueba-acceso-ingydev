var ENCRIPTACION = require("../routes/utilidades/encriptacion");
var DBCONEXION = require('../mod/conexion');
var conexion_db_ON;
var USER = require('../mod/user');
var express = require('express');
var nodemailer = require('nodemailer');
var router = express.Router();
FILECONTROLADOR = {};

FILECONTROLADOR.iniciar = function (socket) {

	// VERIFICAR CONEXION POR URL
	router.get('/', function(req, res) {
		try {
			DBCONEXION.testing_conexion(function (conexion_db) {
				if (conexion_db) {
					res.json({
						mensaje:"Conexión exitosa",
						fecha_conexion : conexion_db
					});
				}else{
					res.json({
						mensaje:"Conexión erronea"
					});
				};
			});
		}catch (error) {
			console.log(error)
		}
	});
	// CUENTA
	router.post('/crear-cuenta', function(req, res) {
		try {
    		console.log(req.body);
			DBCONEXION.iniciar_conexion(function (conexion_db) {
				if (conexion_db!=false) {
					conexion_db_ON = conexion_db;
					USER.verificarEmail(conexion_db_ON,req.body.datos,function (err_v,result_v2) {
						//console.log(err_v,result_v);
						if(result_v2==false)
						{	
							
							USER.crearCuenta(conexion_db_ON,req.body.datos,function (error) {
								if(error==true)
								{
									//DBCONEXION.cerrar_conexion(conexion_db_ON);
									//req.session.error_bot = true,
									res.json({
										error:true,
										mensaje:"Error saving"
									});
								}else{
									//DBCONEXION.cerrar_conexion(conexion_db_ON);
									res.json({
										error:false,
										mensaje:"Tu cuenta ha sido creada!"
									});
								};
							});

						}else{
							//DBCONEXION.cerrar_conexion(conexion_db_ON);
							res.json({
								error:true,
								mensaje:"Ya existe el correo electronico"

							});						
						}
					});
				}
			});
		}catch (error) {
			console.log(error)
		}
	});
	router.post('/actualizar-cuenta', function(req_, res_) {
		try {

			if(req_.body.token){
				let datosToken_ = JSON.parse(ENCRIPTACION.decryptMSS(req_.body.token));
				if (datosToken_.id>0) {
					DBCONEXION.iniciar_conexion(function (conexion_db) {
						if (conexion_db!=false) {
							conexion_db_ON = conexion_db;
							//console.log(datosToken_,req_.body.datos);
					
							USER.verificarEmail(conexion_db_ON,req_.body.datos,function (err_v1,result_v1) {
								//console.log(result_v1[0]);
								if(result_v1==false)
								{	

									USER.editar(conexion_db_ON,datosToken_,req_.body.datos,function(err_0,result_0){
										//console.log(err_0,result_0);
										if(result_0!=true)
										{
											if (err_0==true) {
												res_.json({
													error:true,
													error_m:err_0,
													mensaje:"Error de base de datos, comuniquese con su administrador"
												}); 
											} else {
												res_.json({
													error:true,
													error_m:"Error de base de datos, comuniquese con su administrador",
													mensaje:err_0
												}); 								
											}
											
										}else{
											let password = datosToken_.password;
											if (req_.body.datos.password!="") {
												let newpassword = ENCRIPTACION.encrypt(req_.body.datos.password, req_.body.datos.password);
												//console.log('newpassword',newpassword)
												if (req_.body.datos.oldPassword==ENCRIPTACION.decrypt(datosToken_.password, req_.body.datos.oldPassword)) {
													password = newpassword;
												}
											}
											//console.log("contraseña a guardar en token ", password)
			
											let token_ = ENCRIPTACION.encryptMSS(JSON.stringify({id:req_.body.datos.id,username:req_.body.datos.username,password:password}));							req_.session.token = token_;
											USER.addToken(conexion_db_ON,{
												token:token_,
												id:req_.body.datos.id,
											},function (result_2) {
												if(result_2==false)
												{
													res_.json({
														error : false,
														mensaje : "Guardado correctamente",
														token : token_
													});
												}else{
													res_.json({
														error : true,
														mensaje : "no se pudo guardar el token",
													});
			
												};
											});
			
										}
									});
								
								}else{
									//DBCONEXION.cerrar_conexion(conexion_db_ON);
									res_.json({
										error:true,
										mensaje:"Ya existe el correo electronico"

									});						
								}
							});																		

						}
					});
				}else{
					res_.json({
						error:true,
						mensaje : "No posee la permisologia, token invalido al desifrar",
						token:datosToken_
					});
	
				}
				
			}else{
				res_.json({
					error:true,
					mensaje : "No posee la permisologia, no hay token"
				});
			}

		}catch (error) {
			console.log(error)
		}
	});
	router.post('/cambiar-contrasena', function(req_, res_) {
		try {
			//if (req_.body.serverNodePassword=="1234") {	
				//if(req_.body.token){
					//let datosToken_ = JSON.parse(ENCRIPTACION.decryptMSS(req_.body.token));
					//if (datosToken_.id>0) {	
						DBCONEXION.iniciar_conexion(function (conexion_db) {
							if (conexion_db!=false) {
								conexion_db_ON = conexion_db;
								//console.log(req_.body.datos)
								USER.get_existencia(conexion_db_ON,req_.body.datos,function (err_v,result_v) {
									if(result_v[0])
									{	
										
										// Creamos el codigo temporal
										var nueva_contrasena = ENCRIPTACION.encryptMSS(new Date().toString()).substring(40,48);
										USER.cambiarContrasena(conexion_db_ON,req_.body.datos,nueva_contrasena,function (err, result) {
											if(err==true)
											{
												//req_.session.error_bot = true,
												res_.json({
													error:true,
													mensaje:"Error 		"
												});
											}else{
												//console.log(result);

												// TOdos los datos son correctos
												var transporter = nodemailer.createTransport({
													service: 'gmail',
													auth: {
														user: 'COLOCAR EMAIL DE GMAIL',
														pass: 'COLOCAR CONTRASEÑA DE APP DE CUENTA EN GOOGLE'
													}
												});
												//console.log(req_.body.datos)
												var mailOptions = {
													from: 'COLOCAR EMAIL DE GMAIL',
													to: req_.body.datos.email,
													subject: 'Cambio de contraseña',
													text: 'Saludos '+result_v[0].nombre+' '+result_v[0].apellido+', ' +
														'Esta es su nueva contraseña : '+nueva_contrasena
												};
												transporter.sendMail(mailOptions, function(error, info){
													if (error) {
													console.log(error);
													res_.json({
																error : true,
																error_m : error,
																mensaje : "Error al enviar el correo electronico.",
																data : req_.body.datos
															});									    
													} else {
														res_.json({
																	error : false,
																	status : true,
																	mensaje : "Su contraseña ha sid cambiada y fue enviada a "+req_.body.datos.email+'.',
																	datos : info.response
																});										
														}
												});


												

											};
										});	
													
									}else{
										res_.json({
											error:true,
											status:false,
											mensaje:"Información incorrecta"
										});												
									}
								});					
							}
						});
					/*}else{
						res_.json({
							error:true,
							mensaje : "No posee la permisologia, token invalido al desifrar",
							token:datosToken_
						});

					}*/
					
				/*}else{
					res_.json({
						error:true,
						mensaje : "No posee la permisologia"
					});
				}*/
				
			/*} else {
				res_.json({
					error : true,
					mensaje : "No tiene permiso para acceder.",
				});	
			}*/	
		}catch (error) {
			console.log(error)
		}
	});
	//	SESION
	router.post('/iniciar-sesion', function(req_, res_) {
		try {
			DBCONEXION.iniciar_conexion(function (conexion_db) {
				if (conexion_db!=false) {
					conexion_db_ON = conexion_db;
					USER.get_existencia(conexion_db_ON,req_.body,function(err_0,result_0){
						if(err_0)
						{
							res_.json({
								error:true,
								mensaje:err_0
							}); 
						}else{
							if (result_0==false) {
								res_.json({
											error : true,
											error_m : err_0,
											mensaje : "El usuario no existe"
										});
							}else{
								//console.log(req_.body);
								USER.iniciarSesion(conexion_db_ON,req_.body,result_0[0],function(error,result){
									//console.log(error,result[0]);
									if(error)
									{
										res_.json({
												error : true,
												error_m : error,
												mensaje : "Contraseña incorrecta"
											});
									}else{
	
										if (result==false) {
											res_.json({
														error : true,
														error_m : error,
														mensaje : "Contraseña incorrecta"
													});
										}else{
											let token_ = ENCRIPTACION.encryptMSS(JSON.stringify({id:result[0].id,username:result[0].username,password:result[0].password,}));
											USER.addToken(conexion_db_ON,{
												token:token_,
												id:result[0].id,
											},function (result_2) {
												if(result_2==false)
												{
													res_.json({
														error : false,
														mensaje : "Started session",
														token : token_,
														nombre: result[0].nombre,
														apellido: result[0].apellido,
													});
												}else{
													res_.json({
														error : true,
														mensaje : "no se pudo guardar el token",
													});
	
												};
											});
																				
											
										
										};
										
									}
								});
							};
							
						}
					});	
				};
			});    
		}catch (error) {
			console.log(error)
		}
	});
	router.post('/validar-sesion', function(req_, res_) {
		try {
			//console.log(req_.body)
			if(req_.body.token){
				let datosToken_ = JSON.parse(ENCRIPTACION.decryptMSS(req_.body.token));
				if (datosToken_.id>0) {
					DBCONEXION.iniciar_conexion(function (conexion_db) {
						if (conexion_db!=false) {
							conexion_db_ON = conexion_db;
							//console.log(datosToken_);
							USER.get_u_nombre_id(conexion_db_ON,datosToken_,function(err_0,result_0){
								//console.log(err_0,result_0,req_.body);
								if(err_0)
								{ 
									res_.json({
										error:true,
										error_m:err_0,
										mensaje:"Error de base de datos, comuniquese con su administrador"
									}); 
								}else{
									if (result_0==false) {
										res_.json({
													error : true,
													error_m : err_0,
													mensaje : "El usuario no existe"
												});
									}else{
										if (req_.body.token===result_0[0].token) {
											res_.json({
												error:false,
												mensaje : "Acceso permitido",
												datos: result_0[0]
											});
										}else{
											res_.json({
												error : true,
												error_m : err_0,
												mensaje : "El token no es el correcto"
											});
										}
									}
								}
							});
	
						}
					});
				}else{
					res_.json({
						error:true,
						mensaje : "No posee la permisologia, token invalido al desifrar",
						token:datosToken_
					});
	
				}
				
			}else{
				res_.json({
					error:true,
					mensaje : "No posee la permisologia, no hay token"
				});
			}    
		}catch (error) {
			console.log(error)
		}
	});
	return router;
}	

module.exports = FILECONTROLADOR;