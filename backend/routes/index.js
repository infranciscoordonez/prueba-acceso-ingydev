var ENCRIPTACION = require("./utilidades/encriptacion");
var DBCONEXION = require('../mod/conexion');
var conexion_db_ON;
var express = require('express');
var router = express.Router();
FILECONTROLADOR = {};

FILECONTROLADOR.iniciar = function (socket) {

	router.get('/', function(req, res) {
		try {
			DBCONEXION.testing_conexion(function (conexion_db) {
				if (conexion_db) {
					res.json({
						mensaje:"Conexión exitosa",
						fecha_conexion : conexion_db
					});
				}else{
					res.json({
						mensaje:"Conexión erronea"
					});
				};
			});    
		}catch (error) {
			console.log(error)
		}
	});

	return router;
}

module.exports = FILECONTROLADOR;