var key_master = "contraseñaparaencriptar";
var CryptoJS = require("crypto-js");

var ENCRIPTACION = {};

ENCRIPTACION.encryptMSS = function(text){
  try {
    // Encrypt
    var ciphertext = CryptoJS.AES.encrypt(text, key_master).toString();
    console.log(ciphertext);
    return ciphertext;
  }catch (error) {
      console.log(error)
  }
}
ENCRIPTACION.decryptMSS = function(text){
  try {
    // Decrypt
    var bytes  = CryptoJS.AES.decrypt(text, key_master);
    var originalText = bytes.toString(CryptoJS.enc.Utf8);
    // console.log(originalText);
    return originalText;    
  }catch (error) {
      console.log(error)
  }
}
ENCRIPTACION.encrypt = function(text,key_encriptacion){
  try {
    // Encrypt
    var ciphertext = CryptoJS.AES.encrypt(text, key_encriptacion).toString();
    // console.log(ciphertext);
    return ciphertext;
  }catch (error) {
      console.log(error)
  }
}
ENCRIPTACION.decrypt = function(text, key_desencriptacion){
  try {
    // Decrypt
    console.log(text, key_desencriptacion);
    var bytes  = CryptoJS.AES.decrypt(text, key_desencriptacion);
    var originalText = bytes.toString(CryptoJS.enc.Utf8);
    console.log(originalText);
    return originalText;
  }catch (error) {
      console.log(error)
  }
}

module.exports = ENCRIPTACION;

