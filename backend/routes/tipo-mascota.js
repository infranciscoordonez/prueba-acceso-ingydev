var ENCRIPTACION = require("../routes/utilidades/encriptacion");
var DBCONEXION = require('../mod/conexion');
var conexion_db_ON;
var TIPOMASCOTA = require('../mod/tipo-mascota');
var express = require('express');
var router = express.Router();
FILECONTROLADOR = {};

FILECONTROLADOR.iniciar = function (socket) {

	router.post('/get', function(req, res) {
		try {
			DBCONEXION.iniciar_conexion(function (conexion_db) {
				if (conexion_db!=false) {
					conexion_db_ON = conexion_db;
					TIPOMASCOTA.get_tipos_mascota(conexion_db_ON,function (result) {
						if(!result)
						{
							res.json({
								error:true,
								mensaje:"Error consultando"
							});
						}else{
							res.json({
								error:false,
								datos: result,
								mensaje: (result[0])? '' : 'Todavia no se han realizado registros'
							});
						};
					});
				}
			});
		}catch (error) {
			console.log(error)
		}
	});
	router.post('/registrar', function(req, res) {
		try {
    		console.log(req.body);
			DBCONEXION.iniciar_conexion(function (conexion_db) {
				if (conexion_db!=false) {
					conexion_db_ON = conexion_db;
					TIPOMASCOTA.verificarNombre(conexion_db_ON,req.body,function (err_v,result_v2) {
						//console.log(err_v,result_v);
						if(result_v2==false)
						{	
							
							TIPOMASCOTA.registrar(conexion_db_ON,req.body,function (error) {
								if(error==true)
								{
									res.json({
										error:true,
										mensaje:"Error guardando"
									});
								}else{

									TIPOMASCOTA.get_tipos_mascota(conexion_db_ON,function (result) {
										if(result==false)
										{
											res.json({
												error:true,
												mensaje:"Error consultando"
											});
										}else{
											res.json({
												error:false,
												datos: result,
												mensaj_0 : "¡Se ha realizado el registro!",
												mensaje: (result[0])? '' : 'No se han realizado registros'
				
											});
										};
									});									

								};
							});

						}else{
							res.json({
								error:true,
								mensaje:"Ya existe el dato"

							});						
						}
					});
				}
			});
		}catch (error) {
			console.log(error)
		}
	});
	router.post('/editar', function(req_, res_) {
		try {

			if(req_.body.token){
				let datosToken_ = JSON.parse(ENCRIPTACION.decryptMSS(req_.body.token));
				if (datosToken_.id>0) {
					DBCONEXION.iniciar_conexion(function (conexion_db) {
						if (conexion_db!=false) {
							conexion_db_ON = conexion_db;
							//console.log(datosToken_,req_.body.datos);
					
							TIPOMASCOTA.verificarNombre(conexion_db_ON,req_.body,function (err_v1,result_v1) {
								//console.log(result_v1[0]);
								if(result_v1==false)
								{	

									TIPOMASCOTA.editar(conexion_db_ON,datosToken_,req_.body,function(err_0,result_0){
										//console.log(err_0,result_0);
										if(result_0!=true)
										{
											if (err_0==true) {
												res_.json({
													error:true,
													error_m:err_0,
													mensaje:"Error de base de datos, comuniquese con su administrador"
												}); 
											} else {
												res_.json({
													error:true,
													error_m:"Error de base de datos, comuniquese con su administrador",
													mensaje:err_0
												}); 								
											}
											
										}else{


											TIPOMASCOTA.get_tipos_mascota(conexion_db_ON,function (result) {
												if(result==false)
												{
													res.json({
														error:true,
														mensaje:"Error consultando"
													});
												}else{
													res.json({
														error:false,
														datos: result,
														mensaj_0 : "Guardado correctamente",
														mensaje: (result[0])? '' : 'No se han realizado registros'
						
													});
												};
											});

										}
									});
								
								}else{
									//DBCONEXION.cerrar_conexion(conexion_db_ON);
									res_.json({
										error:true,
										mensaje:"Ya existe el dato"

									});						
								}
							});																		

						}
					});
				}else{
					res_.json({
						error:true,
						mensaje : "No posee la permisologia, token invalido al desifrar",
						token:datosToken_
					});
	
				}
				
			}else{
				res_.json({
					error:true,
					mensaje : "No posee la permisologia, no hay token"
				});
			}

		}catch (error) {
			console.log(error)
		}
	});

	return router;
}	

module.exports = FILECONTROLADOR;