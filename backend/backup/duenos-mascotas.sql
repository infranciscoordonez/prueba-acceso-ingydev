-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.5.4-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para duenos_mascotas
DROP DATABASE IF EXISTS `duenos_mascotas`;
CREATE DATABASE IF NOT EXISTS `duenos_mascotas` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci */;
USE `duenos_mascotas`;

-- Volcando estructura para tabla duenos_mascotas.dueno
DROP TABLE IF EXISTS `dueno`;
CREATE TABLE IF NOT EXISTS `dueno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_completo` varchar(50) DEFAULT NULL,
  `email` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla duenos_mascotas.dueno: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `dueno` DISABLE KEYS */;
INSERT INTO `dueno` (`id`, `nombre_completo`, `email`) VALUES
	(1, 'Francisco', 'ing.francisco.ordonez@gmail.com'),
	(2, 'Juan', 'juan@example.com');
/*!40000 ALTER TABLE `dueno` ENABLE KEYS */;

-- Volcando estructura para tabla duenos_mascotas.mascota
DROP TABLE IF EXISTS `mascota`;
CREATE TABLE IF NOT EXISTS `mascota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `edad` int(11) NOT NULL,
  `id_dueno` int(11) NOT NULL,
  `id_tipo_mascota` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_tipo_mascota` (`id_tipo_mascota`),
  KEY `id_dueno` (`id_dueno`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla duenos_mascotas.mascota: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `mascota` DISABLE KEYS */;
INSERT INTO `mascota` (`id`, `nombre`, `edad`, `id_dueno`, `id_tipo_mascota`) VALUES
	(1, 'benjamin', 1, 1, 1),
	(2, 'benjaminr', 1, 1, 1);
/*!40000 ALTER TABLE `mascota` ENABLE KEYS */;

-- Volcando estructura para tabla duenos_mascotas.tipo_mascota
DROP TABLE IF EXISTS `tipo_mascota`;
CREATE TABLE IF NOT EXISTS `tipo_mascota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla duenos_mascotas.tipo_mascota: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_mascota` DISABLE KEYS */;
INSERT INTO `tipo_mascota` (`id`, `nombre`) VALUES
	(1, 'Perro'),
	(2, 'Gato'),
	(3, 'Ave'),
	(4, 'Tortuga');
/*!40000 ALTER TABLE `tipo_mascota` ENABLE KEYS */;

-- Volcando estructura para tabla duenos_mascotas.usuario
DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL DEFAULT '',
  `apellido` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `fecha_ingreso` varchar(50) NOT NULL DEFAULT '',
  `token` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla duenos_mascotas.usuario: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id`, `nombre`, `apellido`, `password`, `email`, `fecha_ingreso`, `token`) VALUES
	(6, 'Francisco', 'Ordoñez', 'U2FsdGVkX19ysdOEb8zaaIEegJI4YQ0EpBTXcUtp9Sw=', 'ing.francisco.ordonez@gmail.com', '2021/02/23 10:37:49 PM', 'U2FsdGVkX1/7xe+rvhTyPB+ZAWP0icVW0h9+HRXcg+PUeCdOCKAmSYVDu6Kea5IMKhn4hg3AiX3tI5g1KlraMDGpjNF6m2nzshiyIjFaQ09QfZ+vgRhmUXQRKrBKHEoM');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
