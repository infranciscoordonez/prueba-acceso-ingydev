var createError = require('http-errors');
var express = require('express');  
var session = require('express-session'); 
var fileUpload = require('express-fileupload')
var path = require('path');
var favicon = require('serve-favicon');
//  var favicon = require('static-favicon');
var logger = require('morgan'); 
var cookieParser = require('cookie-parser');
var contentLength = 999000;

// INSTANCIAR IMPORTACION DE ARCHIVOS
var index_ = require('./routes/index');
var user_ = require('./routes/user');
var tipo_mascota_ = require('./routes/tipo-mascota');
var gestion_mascotas_ = require('./routes/gestion-mascotas');

/*****************************************
 * CONTRUIR BASE DEL SERVIDOR
 *****************************************/
var app = express();
/*****************************************
 * FIN: CONTRUIR BASE DEL SERVIDOR
 *****************************************/
// INICIARLIZAR INSTACIA DE ARCHIVOS IMPORTADOR
var index = index_.iniciar(app);
var user = user_.iniciar(app);
var tipo_mascota = tipo_mascota_.iniciar(app);
var gestion_mascotas = gestion_mascotas_.iniciar(app);

//---------------------------------------------------------

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//  app.use(favicon(path.join(__dirname, 'public', 'favicon.png')));
app.use(logger('dev'));
app.use(logger('common'));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({ limit: '50mb', extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//  Routing server
//  Configurar cabeceras y cors
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Content-Length', contentLength);
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT');//, DELETE
  next();
});

app.use(fileUpload())


app.use('/gestion-mascotas', gestion_mascotas);
app.use('/tipo-mascota', tipo_mascota);
app.use('/usuario', user);
app.use('/', index);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
