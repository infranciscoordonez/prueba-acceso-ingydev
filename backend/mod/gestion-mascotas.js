//creamos la base de datos tienda y el objeto OBJETOMODELO donde iremos almacenando la info
var ENCRIPTACION = require("../routes/utilidades/encriptacion");
var UTILIDAD = require('../mod/utilidad');
var OBJETOMODELO = {};

// Iniciar sesion

OBJETOMODELO.get_dueno_id = function(db,datos,callback)
{
    try {
        //stmt = db.prepare();
        //pasamos el id del cliente a la consulta
        //stmt.bind(); 
        //console.log(datos); 
        var sql =   `SELECT 
                        d.*
                    FROM dueno AS d
                    WHERE d.id =? `;
        db.query(sql,[parseInt(datos.id)]).then( row => {
            
            //retornamos la fila con los datos del usuario
            if(row) 
            {
                //console.log(row);
                callback(false, row);
            }else{
                //El usuario no existe en uneeverso
                callback(true, false);
            }
            
        }).catch(error => {
            console.log(error)
            callback(true, false);
        });    
    }catch (error) {
        console.log(error)
    }
}
OBJETOMODELO.get_mascota_id = function(db,datos,callback)
{
    try {
        //stmt = db.prepare();
        //pasamos el id del cliente a la consulta
        //stmt.bind(); 
        //console.log(datos); 
        var sql =   `SELECT 
                        m.*
                    FROM mascota AS m
                    WHERE m.id =? `;
        db.query(sql,[parseInt(datos.id)]).then( row => {
            
            //retornamos la fila con los datos del usuario
            if(row) 
            {
                //console.log(row);
                callback(false, row);
            }else{
                //El usuario no existe en uneeverso
                callback(true, false);
            }
            
        }).catch(error => {
            console.log(error)
            callback(true, false);
        });    
    }catch (error) {
        console.log(error)
    }
}
OBJETOMODELO.get_duenos = function(db,callback)
{
    try {
        var sql =   `SELECT 
                        d.*
                    FROM dueno AS d `;
        db.query(sql).then( row => {
            callback( row);
            
        }).catch(error => {
            console.log(error)
            callback( false);
        });    
    }catch (error) {
        console.log(error)
    }
}
OBJETOMODELO.get_mascotas = function(db, datos,callback)
{
    try {
        var sql =   `SELECT 
                        m.*
                    FROM mascota AS m
                    WHERE m.id_dueno = ?`;
        db.query(sql, [datos.id_dueno]).then( row => {
            callback( row);
        }).catch(error => {
            console.log(error)
            callback(false);
        });    
    }catch (error) {
        console.log(error)
    }
}
OBJETOMODELO.get_mascotas_con_duenos = function(db, callback)
{
    try {
        var sql =   `SELECT 
								
                        m.nombre AS NOMBRE_MASCOTA,
                        m.edad AS EDAD_MASCOTA,
                        tm.nombre AS TIPO_MASCOTA,
                        d.nombre_completo AS DUENO,
                        d.email AS DUENO_EMAIL
                        
                    FROM mascota AS m
                    LEFT JOIN dueno AS d ON d.id = m.id_dueno
                    LEFT JOIN tipo_mascota AS tm ON tm.id = m.id_tipo_mascota`;
        db.query(sql).then( row => {
            callback( row);
        }).catch(error => {
            console.log(error)
            callback(false);
        });    
    }catch (error) {
        console.log(error)
    }
}
OBJETOMODELO.verificarEmailDueno = function(db,datos,callback)
{
    try {
        //stmt = db.prepare();
        //pasamos el id del cliente a la consulta
        //stmt.bind(); 
        console.log(datos); 
        // SI hay ID, es porque hay un login

            var sql = "SELECT * FROM dueno WHERE email = ? ";
            var datos_array_ = [ datos.email]
        db.query(sql,datos_array_).then( row => {
            
            //retornamos la fila con los datos del usuario
            if(row) 
            {
                console.log(row[0]);
                callback(false, row);
            }else{
                //El usuario no existe en uneeverso
                callback(true, false);
            }
            
        }).catch(error => {
            console.log(error)
            callback(true, false);
        });    
    }catch (error) {
        console.log(error)
    }
}
OBJETOMODELO.verificarNombreMascota = function(db,datos,callback)
{
    try {
        //stmt = db.prepare();
        //pasamos el id del cliente a la consulta
        //stmt.bind(); 
        console.log(datos); 
        // SI hay ID, es porque hay un login

            var sql = "SELECT * FROM mascota WHERE nombre = ? ";
            var datos_array_ = [ datos.nombre]
        db.query(sql,datos_array_).then( row => {
            
            //retornamos la fila con los datos del usuario
            if(row) 
            {
                console.log(row[0]);
                callback(false, row);
            }else{
                //El usuario no existe en uneeverso
                callback(true, false);
            }
            
        }).catch(error => {
            console.log(error)
            callback(true, false);
        });    
    }catch (error) {
        console.log(error)
    }
}
OBJETOMODELO.registrarDueno = function(db,datos_,callback)
{    
    try {
        var sql = "INSERT INTO dueno (id,nombre_completo,email) VALUES (?,?,?) ";
        db.query(
            sql,
        [
            null,
            datos_.nombre_completo,
            datos_.email
        ]).then(res =>{ 
    
            if (res.insertId>0) {
                callback(false);                    
            }else{
                callback(true); 
            };            
                                    
        }).catch(error => {
            console.log(error)
            callback(true);
        });    
    }catch (error) {
        console.log(error)
    }                  
}
OBJETOMODELO.editarDueno = function(db,datosToken,datos_,callback)
{     
    try {
        var sql = "UPDATE dueno SET nombre_completo=?, email=? WHERE id=? ";
        db.query(
            sql,
        [
            datos_.nombre_completo,
            datos_.email,
            datos_.id
        ]).then( res =>{
            
                if (res.affectedRows>0) {
                    callback(false,true);                    
                }else{
                    callback(true,false); 
                };   
            
        }).catch(error => {
            console.log(error)
            callback(true, false);
        });
    }catch (error) {
        console.log(error)
    }              
}
OBJETOMODELO.registrarMascota = function(db,datos_,callback)
{    
    try {
        var sql = "INSERT INTO mascota (id,nombre, edad, id_dueno, id_tipo_mascota) VALUES (?,?,?,?,?) ";
        db.query(
            sql,
        [
            null,
            datos_.nombre,
            datos_.edad,
            datos_.id_dueno,
            datos_.id_tipo_mascota
        ]).then(res =>{ 
    
            if (res.insertId>0) {
                callback(false);                    
            }else{
                callback(true); 
            };            
                                    
        }).catch(error => {
            console.log(error)
            callback(true);
        });    
    }catch (error) {
        console.log(error)
    }                  
}
OBJETOMODELO.editarMascota = function(db,datos_,callback)
{     
    try {
        var sql = "UPDATE mascota SET nombre=?, edad=? WHERE id=? ";
        db.query(
            sql,
        [
            datos_.nombre,
            datos_.edad,
            datos_.id
        ]).then( res =>{
            
                if (res.affectedRows>0) {
                    callback(false,true);                    
                }else{
                    callback(true,false); 
                };   
            
        }).catch(error => {
            console.log(error)
            callback(true, false);
        });
    }catch (error) {
        console.log(error)
    }              
}
//exportamos el modelo para poder utilizarlo con require
module.exports = OBJETOMODELO;