//creamos la base de datos tienda y el objeto USER donde iremos almacenando la info
var ENCRIPTACION = require("../routes/utilidades/encriptacion");
var UTILIDAD = require('../mod/utilidad');
var USER = {};

// Iniciar sesion

USER.get_existencia = function(db,datos,callback)
{
    try {
        //stmt = db.prepare();
        //pasamos el id del cliente a la consulta
        //stmt.bind(); 
        console.log(datos);
        var sql = "SELECT * FROM usuario WHERE email = ? ";
        db.query(sql,[datos.email]).then( row => {
            
            //retornamos la fila con los datos del usuario
            if(row) 
            {
                callback(false, row);
            }else{
                //El usuario no existe en uneeverso
                callback(true, false);
            }
            
        }).catch(error => {
            console.log(error)
            callback(true, false);
        });    
    }catch (error) {
        console.log(error)
    }
}
USER.get_u_nombre_id = function(db,datos,callback)
{
    try {
        //stmt = db.prepare();
        //pasamos el id del cliente a la consulta
        //stmt.bind(); 
        //console.log(datos); 
        var sql =   `SELECT 
                        u.*
                    FROM usuario AS u
                    WHERE u.id =? `;
        db.query(sql,[parseInt(datos.id)]).then( row => {
            
            //retornamos la fila con los datos del usuario
            if(row) 
            {
                //console.log(row);
                callback(false, row);
            }else{
                //El usuario no existe en uneeverso
                callback(true, false);
            }
            
        }).catch(error => {
            console.log(error)
            callback(true, false);
        });    
    }catch (error) {
        console.log(error)
    }
}
USER.verificarEmail = function(db,datos,callback)
{
    try {
        //stmt = db.prepare();
        //pasamos el id del cliente a la consulta
        //stmt.bind(); 
        console.log(datos); 
        // SI hay ID, es porque hay un login

            var sql = "SELECT * FROM usuario WHERE email = ? ";
            var datos_array_ = [ datos.email]
        db.query(sql,datos_array_).then( row => {
            
            //retornamos la fila con los datos del usuario
            if(row) 
            {
                console.log(row[0]);
                callback(false, row);
            }else{
                //El usuario no existe en uneeverso
                callback(true, false);
            }
            
        }).catch(error => {
            console.log(error)
            callback(true, false);
        });    
    }catch (error) {
        console.log(error)
    }
}
USER.crearCuenta = function(db,datos_,callback)
{    
    try {
        var sql = "INSERT INTO usuario (id,nombre,apellido,email,password, fecha_ingreso) VALUES (?,?,?,?,?,?) ";
        db.query(
            sql,
        [
            null,
            datos_.nombre,
            datos_.apellido,
            datos_.email,
            ENCRIPTACION.encrypt(datos_.password, datos_.password),
            UTILIDAD.fecha_y_hora_actual_()
        ]).then(res =>{ 
    
            if (res.insertId>0) {
                callback(false);                    
            }else{
                callback(true); 
            };            
                                    
        }).catch(error => {
            console.log(error)
            callback(true);
        });    
    }catch (error) {
        console.log(error)
    }                  
}
USER.editar = function(db,datosToken,datos_,callback)
{     
    try {
        let password = datosToken.password;
        if (datos_.password!="") {
            if (datos_.oldPassword===ENCRIPTACION.decrypt(datosToken.password, datos_.oldPassword)) {
                password = ENCRIPTACION.encrypt(datos_.password, datos_.password);
            }else{
                callback("Incorrect old password",false);
            }
        }
        console.log("contraseña a guardar ", password)
        var sql = "UPDATE usuario SET nombre=? ,apellido=?, email=? , password=? WHERE id=? ";
        db.query(
            sql,
        [
            datos_.nombre,
            datos_.apellido,
            datos_.email,
            password,
            datos_.id
        ]).then( res =>{
            
                if (res.affectedRows>0) {
                    callback(false,true);                    
                }else{
                    callback(true,false); 
                };   
            
        }).catch(error => {
            console.log(error)
            callback(true, false);
        });
    }catch (error) {
        console.log(error)
    }              
}

USER.iniciarSesion = function(db,datosRest,datosDB,callback)
{
    try {
        if (datosRest.password===ENCRIPTACION.decrypt(datosDB.password, datosRest.password)) {

            var sql = "SELECT * FROM usuario WHERE email = ? AND password = ?";
            db.query(sql,[datosRest.email,datosDB.password])
            .then(  row => {
                //retornamos la fila con los datos del usuario
                if(row) 
                {
                    console.log(row[0]);
                    callback(false, row);
                }else{
                    //El usuario no existe en uneeverso
                    callback(true, false);
                }
                
            }).catch(error => {
                console.log(error)
                callback(true, false);
            });
    
        }else{
            callback(true, false);
        }
    
    }catch (error) {
        console.log(error)
    }
}
USER.cambiarContrasena = function(db,datos_,nueva_contrasena,callback)
{           
    try {
        var sql = "UPDATE usuario SET password=? WHERE email=? ";
        db.query(
            sql,
            [
                ENCRIPTACION.encrypt(nueva_contrasena, nueva_contrasena),
                datos_.email
            ]).then( res =>{
                        
                console.log(datos_,res)
                if (res.affectedRows>0) {
                    callback(false,true);                    
                }else{
                    callback(true,false); 
                };   
            
        }).catch(error => {
            console.log(error)
            callback(true, false);
        });
    }catch (error) {
        console.log(error)
    }             
}
USER.addToken = function(db,datos_,callback)
{
	try {
        var sql = "UPDATE usuario SET token=? WHERE id=? ";
        db.query(sql,
        [
            datos_.token,
            datos_.id,
        ]).then( res =>{
                
            
            console.log(datos_,res)
            if (res.affectedRows>0) {
                    callback(false);                    
                }else{
                    callback(true); 
                };
            
                                    
        }).catch(error => {
            console.log(error)
            callback(true);
        });      
	}catch (error) {
		console.log(error)
	}      

}
//exportamos el modelo para poder utilizarlo con require
module.exports = USER;