//creamos la base de datos tienda y el objeto OBJETOMODELO donde iremos almacenando la info
var ENCRIPTACION = require("../routes/utilidades/encriptacion");
var UTILIDAD = require('../mod/utilidad');
var OBJETOMODELO = {};

// Iniciar sesion


OBJETOMODELO.get_tipo_mascota_id = function(db,datos,callback)
{
    try {
        var sql =   `SELECT 
                        tm.*
                    FROM tipo_mascota AS tm
                    WHERE tm.id =? `;
        db.query(sql,[parseInt(datos.id)]).then( row => {
            if(row) 
            {
                callback(false, row);
            }else{
                callback(true, false);
            }
            
        }).catch(error => {
            console.log(error)
            callback(true, false);
        });    
    }catch (error) {
        console.log(error)
    }
}
OBJETOMODELO.get_tipos_mascota = function(db,callback)
{
    try {
        var sql =   `SELECT 
                        tm.*
                    FROM tipo_mascota AS tm `;
        db.query(sql, []).then( row => {
            callback( row);            
        }).catch(error => {
            console.log(error)
            callback(false);
        });    
    }catch (error) {
        console.log(error)
    }
}
OBJETOMODELO.verificarNombre = function(db,datos,callback)
{
    try {
        //stmt = db.prepare();
        //pasamos el id del cliente a la consulta
        //stmt.bind(); 
        console.log(datos); 
        // SI hay ID, es porque hay un login

            var sql = "SELECT * FROM tipo_mascota WHERE nombre = ? ";
            var datos_array_ = [datos.nombre]
        db.query(sql,datos_array_).then( row => {
            
            //retornamos la fila con los datos del usuario
            if(row) 
            {
                console.log(row[0]);
                callback(false, row);
            }else{
                //El usuario no existe en uneeverso
                callback(true, false);
            }
            
        }).catch(error => {
            console.log(error)
            callback(true, false);
        });    
    }catch (error) {
        console.log(error)
    }
}
OBJETOMODELO.registrar = function(db,datos_,callback)
{    
    try {
        var sql = "INSERT INTO tipo_mascota (id,nombre) VALUES (?,?) ";
        db.query(
            sql,
        [
            null,
            datos_.nombre
        ]).then(res =>{ 
    
            if (res.insertId>0) {
                callback(false);                    
            }else{
                callback(true); 
            };            
                                    
        }).catch(error => {
            console.log(error)
            callback(true);
        });    
    }catch (error) {
        console.log(error)
    }                  
}
OBJETOMODELO.editar= function(db,datos_,callback)
{     
    try {
        var sql = "UPDATE tipo_mascota SET nombre=? WHERE id=? ";
        db.query(
            sql,
        [
            datos_.nombre,
            datos_.id
        ]).then( res =>{
            
                if (res.affectedRows>0) {
                    callback(false,true);                    
                }else{
                    callback(true,false); 
                };   
            
        }).catch(error => {
            console.log(error)
            callback(true, false);
        });
    }catch (error) {
        console.log(error)
    }              
}

//exportamos el modelo para poder utilizarlo con require
module.exports = OBJETOMODELO;