var moment = require('moment-timezone');
//moment().tz("America/Bogota").format('DD-MM-YYYY hh:mm:ss A')
var UTILIDAD = {};

UTILIDAD.fecha_anterior_ = function () {
	try {
		//var d = new Date();
		//return d.getDate()+'/'+d.getMonth()+1+'/'+d.getFullYear();
		return moment().tz("America/Caracas").subtract(1, 'days').format('DD/MM/YYYY')
	}catch (error) {
		console.log(error)
	}
}

UTILIDAD.fecha_actual_ = function () {
	try {
		//var d = new Date();
		//return d.getDate()+'/'+d.getMonth()+1+'/'+d.getFullYear();
		return moment().tz("America/Caracas").format('DD/MM/YYYY')
	}catch (error) {
		console.log(error)
	}
}
UTILIDAD.hora_actual_ = function () {
	try {
		return moment().tz("America/Caracas").format('hh A');
	}catch (error) {
		console.log(error)
	}
} 

UTILIDAD.fecha_y_hora_actual_ = function () {
	try {
		//var d = new Date();
		//return d.getDate()+'/'+d.getMonth()+1+'/'+d.getFullYear();
		//-05:00
		return moment().tz("America/Caracas").format('YYYY/MM/DD hh:mm:ss A')
	}catch (error) {
		console.log(error)
	}	
}


module.exports = UTILIDAD;