var DBCONEXION = {};
var db = false;
const mariadb = require('mariadb');
let opcion =0;
/**
 * 0 - LOCALHOST
 * 1 - DEBUG PROYECT
 * 2 - PRODUCCIÓN
 */
var data_conexion = [{
	host: "localhost",
	port: 3306,
	user: "root",
	password: "1234",
	database: "duenos_mascotas",
	connectionLimit: 60, 
}];

DBCONEXION.iniciar_conexion = function (callback) {
	try {
		if (db==false) {
			db = mariadb.createPool(data_conexion[opcion]); 
		}
		callback(db);    
	}catch (error) {
		console.log(error)
	}

}
DBCONEXION.cerrar_conexion = function (db) {
	try {
		db.end().then(() => {
			db = false;
			console.log('Conexion close.');		
		  }).catch(err => {
			console.error(err);
		});    
	}catch (error) {
		console.log(error)
	}
}
DBCONEXION.testing_conexion = function (callback) {
	try {
    	//console.log(data_conexion[opcion]);
		if (db==false) {

			db = mariadb.createPool(data_conexion[opcion]); 

			var sql = "SELECT now()";
			db.query(sql).then(rows => {

				callback(rows);

			}).catch(err => {
				//console.log(err);
				callback(err);
			});					
		}else{
			var sql = "SELECT now()";
			db.query(sql).then(rows => {

				callback(rows);

			}).catch(err => {
				console.log(err);
				callback(err);
			});		
		}
	}catch (error) {
		console.log(error)
	}
}

module.exports = DBCONEXION;