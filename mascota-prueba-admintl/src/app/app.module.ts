import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { LanguageTranslationModule } from './shared/modules/language-translation/language-translation.module';

import { configService } from './shared/services/config.service';
import { userService } from './shared/services/user.service';
import { tipoMascotaService } from './shared/services/tipo-mascota.service';
import { GestionMascotasService } from './shared/services/gestion-mascotas.service';


@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        LanguageTranslationModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [AppComponent],
    providers: [
        AuthGuard,
        configService,
        userService,
        tipoMascotaService,
        GestionMascotasService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
