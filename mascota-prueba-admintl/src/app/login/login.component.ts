import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { userService } from '../shared/services/user.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {


    emailTxt: string = "";
    passwordTxt: string = "";
    mensajeError: string = "";

    constructor(public router: Router, public user: userService) {}

    ngOnInit() {}

    onLoggedin() {

        
        if (this.emailTxt!="" && this.passwordTxt !="") {
    
            this.user.iniciarSesion(this.emailTxt, this.passwordTxt,
            (result) => {
              if (result) {    
                if (result.error == false) {
                  window.localStorage.setItem('nombre_completo', result.nombre+' '+result.apellido);
                  window.localStorage.setItem('token', result.token);
                  localStorage.setItem('isLoggedin', 'true');
                  window.document.location.href = '/dashboard';
                }else{
                  this.mensajeError = result.message;
                }
    
              }
            });
    
        }else{
          this.mensajeError = 'Campos vacios';
        }

    }
}
