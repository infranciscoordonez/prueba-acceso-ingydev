import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { configService } from './config.service';


@Injectable()
export class GestionMascotasService {

    
    constructor(
        public http: HttpClient,
        public config: configService
    ){
    }

    get_duenos(callback){

        this.http.post(this.config.url_servidor+'/gestion-mascotas/get-duenos',{}
        ).subscribe((result: any) => {
            console.log(result);
            callback(result);
        },(error) => {
        console.log(error);
            callback(false);
        });

    }
    get_mascotas(datos_, callback){

        this.http.post(this.config.url_servidor+'/gestion-mascotas/get-mascotas',
        datos_
        ).subscribe((result: any) => {
            console.log(result);
            callback(result);
        },(error) => {
        console.log(error);
            callback(false);
        });

    }    
    registrar_dueno( datos_, callback){

        this.http.post(this.config.url_servidor+'/gestion-mascotas/registrar-dueno',
        datos_
        ).subscribe((result: any) => {
            console.log(result);
            callback(result);
        },
        (error) => {
            console.log(error);
            callback(false);
        });

    }
    editar_dueno( datos_, callback){

        this.http.post(this.config.url_servidor+'/gestion-mascotas/editar-dueno',
        {
            datos: datos_
        }).subscribe((result: any) => {
            console.log(result);
            callback(result);
        },
        (error) => {
            console.log(error);
            callback(false);
        });


    }
    registrar_mascota( datos_, callback){

        this.http.post(this.config.url_servidor+'/gestion-mascotas/registrar-mascota',
        datos_
        ).subscribe((result: any) => {
            console.log(result);
            callback(result);
        },
        (error) => {
            console.log(error);
            callback(false);
        });

    }
    editar_mascota( datos_, callback){

        this.http.post(this.config.url_servidor+'/gestion-mascotas/editar-mascota',
        {
            datos: datos_
        }).subscribe((result: any) => {
            console.log(result);
            callback(result);
        },
        (error) => {
            console.log(error);
            callback(false);
        });


    }    
}
