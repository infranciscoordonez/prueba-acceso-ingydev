import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { configService } from './config.service';


@Injectable()
export class userService {

    
    constructor(
        public http: HttpClient,
        public config: configService
    ){
    }

    iniciarSesion(email_,password_,callback){

        this.http.post(this.config.url_servidor+'/usuario/iniciar-sesion',
        {
            email:email_,
            password:password_,
        }).subscribe((result: any) => {
            console.log(result);
            callback(result);
        },(error) => {
        console.log(error);
            callback(false);
        });

    }
    validarSesion( token_, callback){

        this.http.post(this.config.url_servidor+'/usuario/validar-sesion',
        {
            token: token_,
        }).subscribe((result: any) => {
            console.log(result);
            callback(result);
        },
        (error) => {
            console.log(error);
            callback(false);
        });

    }
    crearCuenta( datos_, callback){

        this.http.post(this.config.url_servidor+'/usuario/crear-cuenta',
        {
            datos: datos_
        }).subscribe((result: any) => {
            console.log(result);
            callback(result);
        },
        (error) => {
            console.log(error);
            callback(false);
        });


    }
    actualizarCuenta( token_, datos_, callback){

        this.http.post(this.config.url_servidor + '/usuario/actualizar-cuenta',
        {
            token: token_,
            datos: datos_
        }).subscribe((result: any) => {
            console.log(result);
            callback(result);
        },
        (error) => {
            console.log(error);
            callback(false);
        });


    }
}
