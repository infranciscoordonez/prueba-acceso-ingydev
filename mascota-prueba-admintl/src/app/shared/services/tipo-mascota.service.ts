import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { configService } from './config.service';


@Injectable()
export class tipoMascotaService {

    
    constructor(
        public http: HttpClient,
        public config: configService
    ){
    }

    get(callback){

        this.http.post(this.config.url_servidor+'/tipo-mascota/get',{}
        ).subscribe((result: any) => {
            console.log(result);
            callback(result);
        },(error) => {
        console.log(error);
            callback(false);
        });

    }
    registrar( nombreTxt, callback){

        this.http.post(this.config.url_servidor+'/tipo-mascota/registrar',
        {
            nombre: nombreTxt,
        }).subscribe((result: any) => {
            console.log(result);
            callback(result);
        },
        (error) => {
            console.log(error);
            callback(false);
        });

    }
    editar( datos_, callback){

        this.http.post(this.config.url_servidor+'/tipo-mascota/editar',
        {
            datos: datos_
        }).subscribe((result: any) => {
            console.log(result);
            callback(result);
        },
        (error) => {
            console.log(error);
            callback(false);
        });


    }
}
