import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { tipoMascotaService } from '../../shared/services/tipo-mascota.service';
import { GestionMascotasService } from '../../shared/services/gestion-mascotas.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {

    public nombreTxt: string = "";
    public emailTxt: string = "";
    public duenos: Array<dueno> = [];
    public nombreMTxt: string = "";
    public edadMTxt: number = 0;
    public tipoMascotaTxt: number = 0;
    public mascotas: Array<any> = [];
    public tiposMascota: Array<any> = [];
    public mensajeError: string = "";

    constructor(public gestionMascotas: GestionMascotasService, public tiposM: tipoMascotaService) {

        this.gestionMascotas.get_duenos((result) => {
            if (result) {    
                if (result.error == false) {
                    this.duenos = result.datos
                }else{
                    this.mensajeError = result.message;
                }
            }
        });

    }

    ngOnInit() {}

    guardarNuevoDueno(){

        if(this.nombreTxt=="" || this.emailTxt==""){
            return false
        }

        this.gestionMascotas.registrar_dueno({
            token: window.localStorage.getItem('token'),
            nombre_completo: this.nombreTxt,
            email: this.emailTxt,
        },(result) => {
            if (result) {    
                if (result.error == false) {
                this.duenos = result.datos
                }else{
                this.mensajeError = result.message;
                }
    
            }
        });

    }

    public dueno_seleccionado : dueno;
    public nombre_dueno_seleccionado : string;
    gestionarMascotas(dueno){
        this.dueno_seleccionado = dueno;
        this.nombre_dueno_seleccionado = dueno.nombre_completo
        console.log(this.dueno_seleccionado)

        this.gestionMascotas.get_mascotas(
            {
                token: window.localStorage.getItem('token'),
                id_dueno: this.dueno_seleccionado.id,
            }
            ,(result) => {
            if (result) {    
                if (result.error == false) {
                    this.mascotas = result.datos
                    this.tiposMascota = result.tipos_mascota                   
                }else{
                    this.mensajeError = result.message;
                    this.mascotas = []
                    this.tiposMascota = result.tipos_mascota                   

                }
            }
        });


    }

    guardarNuevaMascota(){

        console.log(this.nombreMTxt,this.edadMTxt, this.tipoMascotaTxt)
        if(this.nombreMTxt=="" || this.edadMTxt==0 || this.tipoMascotaTxt==0){
            return false
        }

        this.gestionMascotas.registrar_mascota({
            token: window.localStorage.getItem('token'),
            id_dueno: this.dueno_seleccionado.id,
            nombre: this.nombreMTxt,
            edad: this.edadMTxt,
            id_tipo_mascota: this.tipoMascotaTxt,
        },(result) => {
            if (result) {    
                if (result.error == false) {
                this.mascotas = result.datos
                }else{
                this.mensajeError = result.message;
                }
    
            }
        });
        
    }
}

export class dueno {
    nombre_completo: String;
    email: string;
    id: number;
}