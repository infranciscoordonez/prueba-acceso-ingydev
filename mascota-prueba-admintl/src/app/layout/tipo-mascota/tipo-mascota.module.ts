import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { TipoMascotaRoutingModule } from './tipo-mascota-routing.module';
import { TipoMascotaComponent } from './tipo-mascota.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [TranslateModule, CommonModule, TipoMascotaRoutingModule, FormsModule, ReactiveFormsModule],
    declarations: [TipoMascotaComponent]
})
export class TipoMascotaModule {}
