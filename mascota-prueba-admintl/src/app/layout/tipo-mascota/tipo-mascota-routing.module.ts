import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TipoMascotaComponent } from './tipo-mascota.component';

const routes: Routes = [
    {
        path: '',
        component: TipoMascotaComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TipoMascotaRoutingModule {}
