import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { tipoMascotaService } from '../../shared/services/tipo-mascota.service';

@Component({
    selector: 'app-tipo-mascota',
    templateUrl: './tipo-mascota.component.html',
    styleUrls: ['./tipo-mascota.component.scss'],
    animations: [routerTransition()]
})
export class TipoMascotaComponent implements OnInit {

    public nombreTxt: string = "";
    public tiposMascota: Array<any> = [];
    public mensajeError: string = "";

    constructor(private tipo : tipoMascotaService) {
        this.tipo.get((result) => {
            if (result) {    
              if (result.error == false) {
                this.tiposMascota = result.datos
              }else{
                this.mensajeError = result.message;
              }
  
            }
        });
    }

    ngOnInit() {}

    guardarNuevo(){

        if(this.nombreTxt==""){
            return false
        }

        this.tipo.registrar( this.nombreTxt,(result) => {
            if (result) {    
              if (result.error == false) {
                this.tiposMascota = result.datos
              }else{
                this.mensajeError = result.message;
              }
  
            }
        });
    }
}
