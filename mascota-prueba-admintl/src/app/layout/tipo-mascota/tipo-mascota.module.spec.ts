import { TipoMascotaModule } from './tipo-mascota.module';

describe('TipoMascotaModule', () => {
    let tipoMascotaModule: TipoMascotaModule;

    beforeEach(() => {
        tipoMascotaModule = new TipoMascotaModule();
    });

    it('should create an instance', () => {
        expect(TipoMascotaModule).toBeTruthy();
    });
});
