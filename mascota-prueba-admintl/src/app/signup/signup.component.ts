import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { routerTransition } from '../router.animations';
import { userService } from '../shared/services/user.service';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {



    emailTxt: string = "";
    passwordTxt: string = "";
    nombreTxt: string = "";
    apellidoTxt: string = "";
    mensajeError: string = "";

    constructor(public user: userService) {}

    ngOnInit() {}

    onRegister() {

        if (this.nombreTxt!="" && this.apellidoTxt !="" && this.emailTxt!="" && this.passwordTxt !="") {
    
            this.user.crearCuenta({
                email: this.emailTxt, 
                password: this.passwordTxt,
                nombre: this.nombreTxt, 
                apellido: this.apellidoTxt
            },
            (result) => {
              if (result._body) {
    
                const datos = JSON.parse(result._body);
                if (datos.error == false) {
                  window.localStorage.setItem('token', datos.token);
                  localStorage.setItem('isLoggedin', 'true');
                }else{
                  this.mensajeError = datos.message;
                }
    
              }
            });
    
        }else{
          this.mensajeError = 'Campos vacios';
        }

    }


}
